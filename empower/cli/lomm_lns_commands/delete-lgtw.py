#!/usr/bin/env python3
#
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.
"""delete-lgtw CLI command - LoMM LoRaWAN Network Server (LNS) Manager \
CLI tools.

usage: empower-ctl.py delete-lgtw <options>

Delete LoRaWAN lGTW from the LNS database.

optional arguments:
  -h, --help            show this help message and exit
  -p PROJECT_ID, --project_id PROJECT_ID
                        project id

required named arguments:
  -e LGTW_EUID, --lgtw_euid LGTW_EUID
                        lGTW euid
"""

import uuid
import argparse

from empower.cli import command

DESC = "Delete LoRaWAN lGTW from the LNS database."
PARSER = "pa_delete_lgtws"
EXEC = "do_delete_lgtws"


def pa_delete_lgtws(args, cmd):
    """Delete project parser method."""
    usage = "%s <options>" % command.USAGE.format(cmd)

    parser = argparse.ArgumentParser(usage=usage, description=DESC)

    required = parser.add_argument_group('required named arguments')

    required.add_argument(
        "-e", "--lgtw_euid", required=False,
        help="lGTW euid",
        type=str, dest="lgtw_euid")

    parser.add_argument(
        '-p', '--project_id', help='project id',
        default=None, type=uuid.UUID, dest="project_id")

    (args, leftovers) = parser.parse_known_args(args)

    return args, leftovers


def do_delete_lgtws(gargs, args, _):
    """Delete a project."""
    # if args.project_id:
    #   url = '/api/v1/projects/%s/lns/lgtws/%s' % \
    #       args.project_id, args.lgtw_euid
    # else:
    #   url = '/api/v1/lns/lgtws/%s' % args.lgtw_euid
    if args.lgtw_euid:
        url = '/api/v1/lns/lgtws/%s' % args.lgtw_euid
    else:
        url = '/api/v1/lns/lgtws'
        answer = input(
            "Deleting all the lGTWs registered in LNS, are you sure? [Y/n]")
        if not answer:
            answer = "Y"
        if answer == "Y":
            command.connect(gargs, ('DELETE', url), 204)
        else:
            return
