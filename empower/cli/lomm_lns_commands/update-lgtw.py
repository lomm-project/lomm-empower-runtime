#!/usr/bin/env python3
#
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.
"""update-lgtw CLI command - LoMM LoRaWAN Network Server (LNS) Manager \
CLI tools.

usage: empower-ctl.py update-lgtw <options>

Update a LoRaWAN Gateway in the LNS Database.

optional arguments:
  -h, --help            show this help message and exit
  -n NAME, --name NAME  LNS name
  -d DESC, --desc DESC  LNS description
  -o OWNER, --owner OWNER
                        LNS owner; default=None
  -p PROJECT_ID, --project_id PROJECT_ID
                        The project id

required named arguments:
  -e LGTW_EUID, --lgtw_euid LGTW_EUID
                        LNS euid
"""

import uuid
import argparse

from empower.cli import command

DESC = "Update a LoRaWAN Gateway in the LNS Database."
PARSER = "pa_update_lgtw"
EXEC = "do_update_lgtw"


def pa_update_lgtw(args, cmd):
    """Update lgtw parser method."""
    usage = "%s <options>" % command.USAGE.format(cmd)

    parser = argparse.ArgumentParser(usage=usage, description=DESC)

    required = parser.add_argument_group('required named arguments')

    required.add_argument(
        "-e", "--lgtw_euid",
        help="LNS euid",
        type=str, dest="lgtw_euid")

    parser.add_argument(
        '-n', '--name',
        help='LNS name',
        type=str, dest="name")

    parser.add_argument(
        '-d', '--desc',
        help='LNS description',
        type=str, dest="desc")

    parser.add_argument(
        '-o', '--owner',
        help='LNS owner; default=None',
        default=None, type=str, dest="owner")

    parser.add_argument(
        '-p', '--project_id', help='The project id',
        default=None, type=uuid.UUID, dest="project_id")

    (args, leftovers) = parser.parse_known_args(args)

    return args, leftovers


def do_update_lgtw(gargs, args, _):
    """Update a lgtw."""
    request = {
        "version": "1.0"
    }

    if args.name:
        request["name"] = args.name

    if args.desc:
        request["desc"] = args.desc

    if args.owner:
        request["owner"] = args.owner

    headers = command.get_headers(gargs)

    # if args.project_id:
    #   url = '/api/v1/projects/%s/lns/lgtw/%s' %
    #           (args.project_id,args.lgtw_euid)
    # else:
    #   url = '/api/v1/lns/lgtw/%s' % args.lgtw_euid
    url = '/api/v1/lns/lgtws/%s' % args.lgtw_euid
    response, _ = command.connect(
        gargs, ('PUT', url), 201, request,
        headers=headers)

    location = response.headers['Location']
    tokens = location.split("/")
    euid = tokens[-1]

    print(euid, " lGTW updated to LNS  Database")
