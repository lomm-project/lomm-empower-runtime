#!/usr/bin/env python3
#
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.
"""delete-lenddev CLI command - LoMM LoRaWAN Network Server (LNS) Manager \
CLI tools.

usage: empower-ctl.py delete-lenddev <options>

Delete LoRaWAN lEndDev from the LNS database.

optional arguments:
  -h, --help            show this help message and exit
  -p PROJECT_ID, --project_id PROJECT_ID
                        project id

required named arguments:
  -e DEVEUI, --devEUI DEVEUI
                        lEndDev euid
"""

import uuid
import argparse

from empower.cli import command

DESC = "Delete LoRaWAN lEndDev from the LNS database."
PARSER = "pa_delete_lenddevs"
EXEC = "do_delete_lenddevs"


def pa_delete_lenddevs(args, cmd):
    """Delete project parser method."""
    usage = "%s <options>" % command.USAGE.format(cmd)

    parser = argparse.ArgumentParser(usage=usage, description=DESC)

    required = parser.add_argument_group('required named arguments')

    required.add_argument(
        "-e", "--devEUI", required=False,
        help="lEndDev euid",
        type=str, dest="devEUI")

    parser.add_argument(
        '-p', '--project_id', help='project id',
        default=None, type=uuid.UUID, dest="project_id")

    (args, leftovers) = parser.parse_known_args(args)

    return args, leftovers


def do_delete_lenddevs(gargs, args, _):
    """Delete a project."""
    # if args.project_id:
    #   url = '/api/v1/projects/%s/lns/lenddevs/%s' % \
    #       args.project_id, args.devEUI
    # else:
    #   url = '/api/v1/lns/lenddevs/%s' % args.devEUI
    if args.devEUI:
        url = '/api/v1/lns/lenddevs/%s' % args.devEUI
        command.connect(gargs, ('DELETE', url), 204)
    else:
        url = '/api/v1/lns/lenddevs'
        answer = input(
            "Deleting all the lEndDevs registered in LNS, are you sure? [Y/n]")
        if not answer:
            answer = "Y"
        if answer == "Y":
            command.connect(gargs, ('DELETE', url), 204)
        else:
            return
