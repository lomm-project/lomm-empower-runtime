#!/usr/bin/env python3
#
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.
"""add-lns CLI command - LoMM LNS Discovery Service Manager CLI tools.

usage: empower-ctl.py add-lns <options>

Add new LNS to the discovery service database.

optional arguments:
  -h, --help            show this help message and exit
  -o OWNER, --owner OWNER
                        LNS owner; default=None
  -g LGTWS, --lgtws LGTWS
                        add a list of lGTWs to LNS in LNS Discovery database
  -p PROJECT_ID, --project_id PROJECT_ID
                        project id

required named arguments:
  -n LNS_EUID, --lns_euid LNS_EUID
                        LNS euid
  -d DESC, --desc DESC  LNS description
  -u URI, --uri URI     LNS uri
"""

import uuid
import argparse
import re

from empower.cli import command

DESC = "Add new LNS to the discovery service database."
PARSER = "pa_add_lns"
EXEC = "do_add_lns"


def pa_add_lns(args, cmd):
    """Parse the add-lns command."""
    usage = "%s <options>" % command.USAGE.format(cmd)

    parser = argparse.ArgumentParser(usage=usage, description=DESC)

    required = parser.add_argument_group('required named arguments')

    required.add_argument(
        "-n", "--lns_euid", help="LNS euid",
        required=True,
        type=str, dest="lns_euid")

    required.add_argument(
        '-d', '--desc', help='LNS description',
        required=True,
        type=str, dest="desc")

    required.add_argument(
        '-u', '--uri', help='LNS uri',
        required=True,
        type=str, dest="uri")

    parser.add_argument(
        '-o', '--owner', help='LNS owner; default=None',
        type=str, dest="owner")

    parser.add_argument(
        '-g', '--lgtws',
        help='add a list of lGTWs to LNS in LNS Discovery database',
        type=str, dest="lgtws")

    parser.add_argument(
        '-p', '--project_id', help='project id',
        type=uuid.UUID, dest="project_id")

    (args, leftovers) = parser.parse_known_args(args)

    return args, leftovers


def do_add_lns(gargs, args, _):
    """Add a new Project."""
    print(args.lgtws)
    request = {
        "version": "1.0",
        "desc": args.desc,
        "uri": args.uri
    }

    if args.lgtws:
        lgtws = re.sub(r'[\[\]\'\"]', '', args.lgtws)
        lgtws = list(lgtws.split(","))
        request["lgtws"] = lgtws

    if args.owner:
        request["owner"] = args.owner

    headers = command.get_headers(gargs)

    # if args.project_id:
    #   url = '/api/v1/projects/%s/lnss' % args.project_id
    # else:
    #   url = '/api/v1/lnsd/lnss'
    url = '/api/v1/lnsd/lnss/%s' % args.lns_euid
    response, _ = command.connect(
        gargs, ('POST', url), 201, request,
        headers=headers)

    location = response.headers['Location']
    tokens = location.split("/")
    euid = tokens[-1]

    print(euid, " LNS added to LNS Discovery Database")
