#!/usr/bin/env python3
#
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.
"""update-lnss CLI command - LoMM LNS Discovery Service Manager CLI tools.

usage: empower-ctl.py update-lns <options>

Update an existing LNS in the discovery service database.

optional arguments:
  -h, --help            show this help message and exit
  -o OWNER, --owner OWNER
                        LNS owner; default=None
  -g LGTWS, --lgtws LGTWS
                        list of lGTWs to be updated in the discovery service
                        database (default add)
  --del-lgtws           delete a list of lGTWs from the LNS in the discovery
                        service database, if no lgtw is specified, delete ALL
                        lGTWs from the LNS in the discovery service
                        database
  -p PROJECT_ID, --project_id PROJECT_ID
                        project id

required named arguments:
  -n LNS_EUID, --lns_euid LNS_EUID
                        LNS euid
  -d DESC, --desc DESC  LNS description
  -u URI, --uri URI     LNS uri
"""

import uuid
import argparse
import re

from empower.cli import command

DESC = "Update an existing LNS in the discovery service database."
PARSER = "pa_update_lns"
EXEC = "do_update_lns"


def pa_update_lns(args, cmd):
    """Update LNS parser method."""
    usage = "%s <options>" % command.USAGE.format(cmd)

    parser = argparse.ArgumentParser(usage=usage, description=DESC)

    required = parser.add_argument_group('required named arguments')

    required.add_argument(
        "-n", "--lns_euid", help="LNS euid",
        required=True,
        type=str, dest="lns_euid")

    required.add_argument(
        '-d', '--desc', help='LNS description',
        type=str, dest="desc")

    required.add_argument(
        '-u', '--uri', help='LNS uri',
        type=str, dest="uri")

    parser.add_argument(
        '-o', '--owner', help='LNS owner; default=None',
        type=str, dest="owner")

    parser.add_argument(
        '-g', '--lgtws',
        help='list of lGTWs to be updated in the \
            discovery service database (default add)',
        type=str, dest="lgtws")

    parser.add_argument(
        '--del-lgtws',
        help='delete a list of lGTWs from the LNS in the \
            discovery service database, if no lgtw is specified, \
            delete ALL lGTWs from the LNS in the discovery \
            service database',
        action="store_true",
        default=False, dest="del_lgtws")

    parser.add_argument(
        '-p', '--project_id', help='project id',
        type=uuid.UUID, dest="project_id")

    (args, leftovers) = parser.parse_known_args(args)

    return args, leftovers


def do_update_lns(gargs, args, _):
    """Update an existing LNS entry in the database."""
    url = '/api/v1/lnsd/lnss/%s/lgtws/' % args.lns_euid

    if args.del_lgtws:
        if args.lgtws:
            lgtws = re.sub(r'[\[\]\'\"]', '', args.lgtws)
            lgtws = list(lgtws.split(","))
            for lgtw in lgtws:
                command.connect(gargs, ('DELETE', url + str(lgtw)), 204)
                print(
                    "lGTW %s deleted in the LNS in the LNS Discovery Database"
                    % str(lgtw)
                )
        else:
            command.connect(gargs, ('DELETE', url), 204)
            print("Deleted all lGTWS in the LNS in the LNS Discovery Database")
    else:
        request = {
            "version": "1.0",
            "desc": args.desc,
            "uri": args.uri
        }

        if args.lgtws:
            lgtws = re.sub(r'[\[\]\'\"]', '', args.lgtws)
            lgtws = list(lgtws.split(","))
            request["lgtws"] = lgtws

        if args.owner:
            request["owner"] = args.owner

        headers = command.get_headers(gargs)

        # if args.project_id:
        #   url = '/api/v1/projects/%s/lnss' % args.project_id
        # else:
        #   url = '/api/v1/lnsd/lnss'
        url = '/api/v1/lnsd/lnss/%s' % args.lns_euid
        response, _ = command.connect(
            gargs, ('PUT', url), 201, request,
            headers=headers)

        location = response.headers['Location']
        tokens = location.split("/")
        euid = tokens[-1]

        print(euid, " LNS updated to LNS Discovery Database")
