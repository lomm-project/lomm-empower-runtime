#!/usr/bin/env python3
#
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.
"""list-lnss CLI command - LoMM LNS Discovery Service Manager CLI tools.

usage: empower-ctl.py list-lnss <options>

List LoRaWAN LNSs in the discovery service.

optional arguments:
  -h, --help            show this help message and exit
  -l, --lgtws           list lGTWs in the discovery service database
  -n LNS_EUID, --lns_euid LNS_EUID
                        show results for the specified LNS id only
  -g LGTW_EUID, --lgtw_euid LGTW_EUID
                        show results for the specified lGTW id only
  -p PROJECT_ID, --project_id PROJECT_ID
                        project id
"""

import uuid
import argparse

from empower.cli import command

DESC = "List LoRaWAN LNSs in the discovery service."
PARSER = "pa_list_lnss"
EXEC = "do_list_lnss"


def pa_list_lnss(args, cmd):
    """List LNSs parser method."""
    usage = "%s <options>" % command.USAGE.format(cmd)

    parser = argparse.ArgumentParser(usage=usage, description=DESC)

    # required = parser.add_argument_group('required named arguments')

    parser.add_argument(
        '-l', '--lgtws', help='list lGTWs in the discovery service database',
        action="store_true",
        default=False, dest="lgtws")

    parser.add_argument(
        '-n', '--lns_euid', help='show results for the specified LNS id only',
        type=str, dest="lns_euid")

    parser.add_argument(
        '-g', '--lgtw_euid',
        help='show results for the specified lGTW id only',
        type=str, dest="lgtw_euid")

    parser.add_argument(
        '-p', '--project_id', help='project id',
        type=uuid.UUID, dest="project_id")

    (args, leftovers) = parser.parse_known_args(args)

    return args, leftovers


def do_list_lnss(gargs, args, _):
    """List LNSs in the discovery service database."""
    # if args.project_id:
    #   url = '/api/v1/projects/%s/lnss' % args.project_id
    # else:
    #   url = '/api/v1/lnsd/lnss'
    url = '/api/v1/lnsd/lnss'

    if args.lgtws:
        if args.lns_euid:
            url += '/' + args.lns_euid + '/lgtws'
        else:
            url += '//lgtws'

        if args.lgtw_euid:
            url += '/' + args.lgtw_euid

        _, data = command.connect(gargs, ('GET', url), 200)

        if args.lns_euid:
            print(data)
        else:
            for entry in data:
                value = "None"
                if entry['lgtw_euid']:
                    value = "','".join(entry['lgtw_euid'])
                out = "lns_euid:'" + entry['lns_euid'] + "'"
                out += " lgtw_euid:'" + value + "'"
                print(out)
    elif args.lns_euid:
        url += "/" + args.lns_euid
        _, data = command.connect(gargs, ('GET', url), 200)
        out = ""
        for key in entry:
            value = "None"
            if entry[key]:
                if isinstance(entry[key], list):
                    value = "'" + ",'".join(entry[key]) + "'"
                else:
                    value = "'" + str(entry[key]) + "'"
            out += key + ":" + value + " "
        print(out)
    else:
        _, data = command.connect(gargs, ('GET', url), 200)
        out = {}
        for entry in data:
            out = ""
            for key in entry:
                value = "None"
                if entry[key]:
                    if isinstance(entry[key], list):
                        value = "'" + ",'".join(entry[key]) + "'"
                    else:
                        value = "'" + str(entry[key]) + "'"
                out += key + ":" + value + " "
            print(out)
