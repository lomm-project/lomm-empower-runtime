
# LoMM - LoRaWAN Monitoring and Management


### What is LoMM?

LoMM is a platform for monitoring and management of LoRa access network, and allows the monitoring and management of prototype LoRaWAN networks.
It targets research and experimentation in the LoRaWAN domain and it is powered by the open-source, software-defined  5G-Empower.

Checkout LoMM [wiki](https://gitlab.fbk.eu/ccosta/lomm-empower-runtime/wikis/home) for more information.


### LoMM Top-Level Features
* Connects to SEMTECH Basic Station Gateways (Packet Forwarder and TTN next step)
* Northbound abstractions for LoRaWAN events.
* REST API and native (Python) API for accessing the Northbound abstractions
* Flexible southbound interface supporting Basic Station LNS protocol (and udp Packet Forwarder, working)

LoMM is currently under development and more features will be added as they become available.


### What is 5G-EmPOWER?
5G-EmPOWER is a mobile network operating system designed for heterogeneous wireless/mobile networks It is being continuously improved and extended by the SenSe research unit at Fondazione Bruno Kessler (FBK).

Checkout out [5G-EmPOWER website](http://5g-empower.io/) and [5G-EmPOWER wiki](https://github.com/5g-empower/5g-empower.github.io/wiki),
for more information on 5G-EmPOWER.

This repository includes the version of 5G-EmPOWER controller currently integrated with the development version LoMM.

For lastest stable version of 5G-EmPOWER, please refer to the official [repo on github](https://github.com/5g-empower)

### Controller set up:

Please refer to the following page of the 5G-Empower Wiki: ["Setting up the Controller"](https://github.com/5g-empower/5g-empower.github.io/wiki/Setting-up-the-Controller)

### Publications

If you find this code useful in your research, please consider citing:

Cristina E. Costa, Marco Centenaro and Roberto Riggio,
"LoMM: a Monitoring and Management Platform for LoRaWAN Experimentation,"
2020 IEEE International Conference on Communications Workshops (4th Workshop on Convergent Internet of Things (C-IoT)),
Dublin, Ireland, June 2020.

### License
Code is released under the Apache License, Version 2.0.

### Want to collaborate?

Drop an email to ccosta@fbk.eu


